import  React, {useState} from 'react';
import Table  from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {makeStyles} from "@material-ui/core";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import InputBase from '@mui/material/InputBase';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import {useSelector} from "react-redux";
import RemoveModal from "./RemoveCurrency"
import BasicModal from "./CreateModal"

const useStyles = makeStyles((theme)=>({
    container:{
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-end",
        marginTop:100,
        background: "#1f233f",
    },
    coloring:{
        color: "white",
        background: "#1f233f",
    }
}));






export function BasicTable() {
    const classes = useStyles();

    const rows = useSelector((state)=>{
        return state.currencies
    });
    const [indexState, setIndex] = useState(null);
    const [openRemove, setOpenRemove] = useState(false);
    const [openEdit, setOpenEdit] = useState(false);
    const [text, setText] = React.useState("");

    function handleIndexAndRemove(ind){
        setIndex(ind);
        setOpenRemove(true)
    }

    function handleIndexAndEdit(ind){
        setIndex(ind);
        setOpenEdit(true);
        setText("Edit Currency")
    }

    function closeRemove() {
        setIndex(null);
        setOpenRemove(false)

    }

    function closeEdit() {
        setIndex(null);
        setOpenEdit(false)

    }

    return (
        <TableContainer  className = {classes.container}>
            <Paper
                component="form"
                sx={{ p: '2px 4px', display: 'flex', alignItems: 'left',width: 400, height: 30, backgroundColor: "#1b1e37", mt:3, mr: 3}}
            >
                <IconButton type="submit" sx={{ p: '10px' }} aria-label="search">
                    <SearchIcon sx={{color: "white"}} />
                </IconButton >
                <InputBase
                    sx={{ ml: 1, flex: 1, color: "white" }}
                    placeholder="Search currency by Currency name"
                />

            </Paper>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead className = {classes.coloring}>
                    <TableRow >
                        <TableCell sx={{color: "white"}}> ID </TableCell>
                        <TableCell sx={{color: "white"}}> Currency Name </TableCell>
                        <TableCell sx={{color: "white"}}> Rate(1S = X rate) </TableCell>
                        <TableCell/>
                    </TableRow>
                </TableHead>
                <TableBody className = {classes.coloring}>
                    {rows.map((row, index) => (
                        <TableRow key={row.id}>
                            <TableCell component="th" scope="row" sx={{color: "white"}}>
                                {row.id}
                            </TableCell>
                            <TableCell sx={{color: "white"}}>{row.name}</TableCell>
                            <TableCell sx={{color: "white"}}>{row.rate}</TableCell>
                            <TableCell>
                                <div style={{display: "flex", justifyContent: "flex-end"}}>
                                    <EditIcon sx={{color: "green", fontSize: 20, cursor: "pointer"}} onClick={()=>handleIndexAndEdit(index)}/>
                                    <DeleteIcon sx={{color: "red", fontSize: 20, cursor: "pointer", ml:1}} onClick={()=>handleIndexAndRemove(index)} />
                                </div>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <BasicModal index={indexState} handleClose = {closeEdit} open={openEdit} text ={text} />
            <RemoveModal index={indexState} remove = {closeRemove} open={openRemove}/>
        </TableContainer>
    );
}




