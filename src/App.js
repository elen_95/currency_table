import React, {useEffect} from "react"
import Title from "./Title"
import { BasicTable} from "./Table"
import {useDispatch} from "react-redux"
import {changeCurrencies} from "./store/action"


function App() {
    const dispatch = useDispatch();


    useEffect(()=>{
        const data = JSON.parse(localStorage.getItem("currencies"));
        dispatch(changeCurrencies(data))
    },[dispatch]);

  return (
      <>
          <Title/>
          <BasicTable/>
      </>
  );
}

export default App;


