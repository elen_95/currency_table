import React , {useState} from "react"
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import InputBase from '@mui/material/InputBase';
import InputLabel from '@mui/material/InputLabel'
import {makeStyles} from '@material-ui/core/styles';
import {changeCurrencies} from "./store/action";
import {useDispatch, useSelector} from "react-redux"



const useStyles = makeStyles((theme)=>({
    button:{
        width: "100px",
        height: "30px",
        color: "white",
        background: "#44bafb"
    },
    modalStyle: {
        color: "white",
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 460,
        height: 290,
        backgroundColor: '#1f233f',
        padding: 10,
        display: "flex",
        flexDirection: "column",
        boxSizing: "border-box"
    },
    inputStyle:{
        width:435,
        color: "white",
        height: 30,
        backgroundColor: "#1b1e37",
        paddingLeft: "15px"
    },
    buttonCancel:{
        color: "red"
    }
}));





 function BasicModal({handleClose, open, text, index}) {
     const dispatch = useDispatch();
    const [name, setName] = useState("");
    const [rate, setRate] = useState("");

    const classes = useStyles();

    function itemNameHandler(e) {
        setName(e.target.value);
        }

     function itemRateHandler(e) {
         if (!isNaN(+e.target.value)){
             setRate(e.target.value);
         }
     }


     const rows = useSelector((state)=>{
         return state.currencies
     });

     function addToLS() {
         if (name && rate){
         if (text === "Edit Currency" ){
             const editObj = {
                 id: rows[index].id,
                 name: name,
                 rate: rate
             };
             const oldItems = JSON.parse(localStorage.getItem("currencies"))
             oldItems[index] = editObj;
             localStorage.setItem("currencies", JSON.stringify(oldItems));
             dispatch(changeCurrencies(oldItems));
             handleClose();
             setName("");
             setRate("")
         }
         else {
             const obj = {
                 id: rows.length? rows.map((item)=>item.id).sort((a,b)=>b-a)[0]+1: 1,
                 name: name,
                 rate: rate
             };

             const oldCurrencies = JSON.parse(localStorage.getItem("currencies")) || [];
             oldCurrencies.push(obj);
             localStorage.setItem("currencies", JSON.stringify(oldCurrencies))
             dispatch(changeCurrencies(oldCurrencies))
             handleClose()
             setName("")
             setRate("")
         }

     }
     }




     return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}

            >
                <Box className={classes.modalStyle}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                       {text}

                    </Typography>
                    <div  style={{ marginTop: "20px"}}>
                        <InputLabel sx={{color: "white", fontSize: 13, mb:"5px"}} >
                            Name
                        </InputLabel>
                        <InputBase onChange={itemNameHandler}
                            sx={{color: "white", fontSize:13}}
                            className={classes.inputStyle}
                            placeholder="Enter name"
                        />
                    </div>

                    <div  style={{ marginTop: "20px"}}>
                        <InputLabel sx={{color: "white", fontSize: 13, mb:"5px"}}>
                            Rate
                        </InputLabel>
                        <InputBase onChange={itemRateHandler}
                            sx={{color: "white", fontSize:13}}
                            className={classes.inputStyle}
                            placeholder="Enter rate"
                                   value={rate}
                        />
                    </div>
                    <Typography sx={{fontSize:10, opacity: 0.5, pl: 1, pt: 1}}>
                        Enter only number
                    </Typography>
                    <Typography sx={{ mt: 5, display: "flex", justifyContent: "flex-end", color: "#44bafb"}}>
                    <Button variant = "primary" className={classes.buttonCancel} onClick={handleClose}>Cancel</Button>
                    <Button variant = "contained" className={classes.button} onClick={addToLS}>CONFIRM</Button>
                    </Typography>

                </Box>
            </Modal>
        </div>
    );
}


export default BasicModal;