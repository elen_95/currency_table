
const initialState = {
    currencies: [],
};



export default function RootReducer(state = initialState, action){
    switch (action.type) {
        case 'update':
            return {
                ...state,
                currencies: action.payload
            };
        default:
            return state

    }
}