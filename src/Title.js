import React, {useState}from "react"
import Button from '@material-ui/core/Button';
import { AppBar, Container, Toolbar , Typography } from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import BasicModal from "./CreateModal"


const useStyles = makeStyles((theme)=>({
    button:{
        color: "white",
        background: "#44bafb"
    },
    title:{
        background: "#1b1e37",
        flexGrow: 1
    }
}));


function Title (){

    const [open, setOpen] = useState(false);
    const [text, setText] = useState("");

    function handleOpen() {
        setOpen(true);
        setText("Create Currency")

    }
    const handleClose = () =>{
        setOpen(false)
    };


    const classes = useStyles();
    return(
        <>

        <AppBar position = "fixed" className = {classes.title} >
            <Container fixed>
                <Toolbar>
                    <Typography variant = "h5" className = {classes.title}>
                        Custom Currencies
                    </Typography>
                    <Button variant = "contained" className={classes.button} onClick={()=>{handleOpen()}}>ADD CURRENCY</Button>
                </Toolbar>
            </Container>
            <BasicModal open = {open} text={text} handleClose = {handleClose}/>
        </AppBar>
            </>
    )
}

export default Title
