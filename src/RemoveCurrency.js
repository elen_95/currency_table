import React from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import {makeStyles} from '@material-ui/core/styles';
import {changeCurrencies} from "./store/action";
import {useDispatch} from "react-redux";



const useStyles = makeStyles((theme)=>({
    button:{
        width: "90px",
        height: "28px",
        color: "white",
        background: "#44bafb"
    },
    modalStyle: {
        color: "white",
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 440,
        height: 150,
        backgroundColor: '#1f233f',
        padding: 10,
        display: "flex",
        flexDirection: "column",
        boxSizing: "border-box"
    },
    inputStyle:{
        width:4,
        color: "white",
        height: 30,
        backgroundColor: "#1b1e37",
        paddingLeft: "15px"
    }
}));





export default function RemoveModal({index, remove, open}) {
    const classes = useStyles();
    const dispatch = useDispatch();


    function removeItem(){
        let oldItems = JSON.parse(localStorage.getItem("currencies"));
        oldItems.splice(index,1);
        localStorage.setItem("currencies", JSON.stringify(oldItems));
        dispatch(changeCurrencies(oldItems))
        remove()
    }
    return (
        <div>
            <Modal
                open={open}
                onClose={remove}
            >
                <Box className={classes.modalStyle}>
                    <Typography id="modal-modal-title" variant="h6" component="h2" sx={{fontSize: 17}}>
                            Remove Currency
                    </Typography>
                    <Typography sx={{fontSize: 11, pt:2}}>
                        Are you sure you want to remove this currency
                    </Typography>
                    <Typography sx={{ mt: 5, display: "flex", justifyContent: "flex-end", color: "#44bafb"}}>
                        <Button variant = "primary" sx={{fontSize:13}} onClick={remove}>Cancel</Button>
                        <Button variant = "contained" sx={{fontSize:13}} className={classes.button} onClick={removeItem}>CONFIRM</Button>
                    </Typography>

                </Box>
            </Modal>
        </div>
    );
}